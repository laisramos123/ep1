#ifndef RESFRIADOS_H_
#define RESFRIADOS_H_

#include "produto.hpp"

class Resfriados: public Produto{
    private:
         
    public:
        Resfriados();
        Resfriados(string nome,double preco,int quantidade,string categoria,int id);
        ~Resfriados();
        void registrar_Produtos(Resfriados resfriado);
};
#endif