#ifndef ENLATADOS_H_
#define ENLATADOS_H_

#include "produto.hpp"

class Enlatados: public Produto{
    private:
         
    public:
        Enlatados();
        Enlatados(string nome,double preco,int quantidade,string categoria,int id);
        ~Enlatados();
        void registrar_Produtos(Enlatados enlatado);
};
#endif