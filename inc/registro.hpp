#ifndef REGISTRO_HPP_
#define REGISTRO_HPP_
#include <iostream>
#include <string.h>
#include <vector>
#include <fstream>

#include "cliente.hpp"
#include "carrinho.hpp"
#include"funcoes.hpp"
 
class Registro{
        private:
                
                vector <Cliente*> clientes;
                int numero_de_clientes;
        public:
                Registro(vector<Cliente *> lista_de_clientes);
                ~Registro();
                 
                int get_numero_de_clientes();
                
                vector < Cliente * >  trato_de_dados_cliente(vector<Cliente *> clientes);
                
                string lista_de_clientes_atualizada(int tamanho2);
                 
                int retorna_tamanho();
};
#endif