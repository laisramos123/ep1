#ifndef PRODUTO_H_
#define PRODUTO_H_

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>

using namespace std;

class Produto{
    private:
        //Atributos
        string nome;
        double preco;
        int quantidade;
        string categoria;
        int id;
        
    protected:
        int quantidade_no_estoque;
       

    public:
        Produto();
        Produto(string nome,double preco,int quantidade,string categoria,int id);
        ~Produto();
        string get_nome();
        void set_nome(string nome);
        double get_preco();
        void set_preco(double preco);
        int get_quantidade();
        void set_quantidade(int quantidade);
        string get_categoria();
        void set_categoria(string categoria);
        int get_id();
        void set_id(int id);
         

        virtual void registrar_Produtos(Produto produto) ;
        
         
         
};
#endif