#ifndef EMPACOTADOS_H_
#define EMPACOTADOS_H_

#include "produto.hpp"

class Empacotados: public Produto{
    private:
         
    public:
        Empacotados();
        Empacotados(string nome,double preco,int quantidade,string categoria,int id);
        ~Empacotados();
        void registrar_Produtos(/*vector<Produto * >& lista_de_produtos*/ Empacotados empacotado);
};
#endif