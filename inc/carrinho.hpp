#ifndef CARRINHO_H_
#define CARRINHO_H_

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include<iterator>
#include <fstream>
#include"produto.hpp"
#include"enlatados.hpp"
#include"resfriados.hpp"
#include "empacotados.hpp"

 

using namespace std;

class Carrinho{
    private:
        double total;
        vector <Produto* > produtos;
    public:
        Carrinho();
        ~Carrinho() ;
        double get_total();
        void set_total(double total);
        
        void finalizar_compra(int quant );
        //vector<Produto> get_produtos();
        void adicionar_produtos(Produto &produto, int quant );
        void cancelar_Compra( );
};



#endif