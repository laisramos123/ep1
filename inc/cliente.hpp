#ifndef CLIENTE_H_
#define CLIENTE_H_

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include<iterator>
#include <fstream>

 
 
using namespace std;

class Cliente{
    private:
        string nome;
        string telefone;
        string cpf;
         
         
        
    public:
    
        Cliente();
        Cliente(string nome, string telefone, string cpf);
        
        ~Cliente();
        string get_nome();
        void set_nome(string nome);
        string get_telefone();
        void set_telefone(string telefone);
        string get_cpf();
        void set_cpf(string cpf);
         
       
        
        
       
        bool verificar_registro(vector<Cliente*>&  lista_de_clientes );
        void cadastro_De_Clientes(Cliente cliente);
        
        

     
};
#endif