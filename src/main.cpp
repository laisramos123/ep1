#include <iostream>
#include <string>
#include <vector>
#include <string.h>
#include <fstream>

#include "produto.hpp"
#include "cliente.hpp"
#include "carrinho.hpp"
#include "enlatados.hpp"
#include "empacotados.hpp"
#include "resfriados.hpp"
#include "registro.hpp"
 


using namespace std;
int main(){
    vector<Produto * > lista_de_produtos;
    vector<Cliente *> lista_de_clientes;
    
     
    int var=1;
    string str1="enlatados";
    string str2="empacotados";
    string str3="resfriados";
     
    int tamanho=0;
    string nome_aux;
    string telefone_aux;
    string cpf_aux;
    int tamanho2;
  
    while (var)
    {
    Registro * registro;
    registro = new Registro(lista_de_clientes);
    tamanho2=lista_de_clientes.size()+1;
    
    tamanho=registro->retorna_tamanho();
    for (int i=0;i<tamanho;i++)
    {   nome_aux="nome registrado";
        cpf_aux=registro->lista_de_clientes_atualizada(tamanho2);
        telefone_aux="telefone registrado";
        lista_de_clientes.push_back(new Cliente(nome_aux,telefone_aux,cpf_aux));
         
        
          
    }
     
 
    
     
    for(int i=0;i<4;i++){
        cout<<"\t\t******************************************\n"<<endl;
        cout<<"\t\t* Bem vindo ao nosso sistema de compras! *\n"<<endl;
        cout<<"\t\t* Escolha o modo com que deseja operar:  *\n"<<endl;
        cout<<"\t\t******************************************\n"<<endl;
        cout<<"\t\t*              1-Modo venda              *\n"<<endl;
        cout<<"\t\t*              2-Modo estoque            *\n"<<endl;
        cout<<"\t\t*              3-Modo recomendacao       *\n"<<endl;
         
        cout<<"\t\t******************************************\n"<<endl;
        
        int opcao;
        cout<<"Opcao selecionada:";
        cin>>opcao;

        system("clear");
        switch (opcao)
        {
            case 1:{
                
                
                string dado1_Nome_cliente;
                string dado2_Cpf;
                string dado3_Telefone;
                int cliente_cadastrado;
                int j=0,k=1,quant=0;
                //char num='0';

                //Registro de cliente
                    
                     
                    cout<<"Digite os respectivos dados:"<<endl;
                    cout<<"Nome:";
                    cin>>dado1_Nome_cliente;
                    cout<<"CPF:";
                    cin>>dado2_Cpf;
                    cout<<"Telefone: ";
                     
                     
                    cin>>dado3_Telefone;
                    Cliente * cliente1;
                    cliente1 = new Cliente(dado1_Nome_cliente,dado2_Cpf,dado3_Telefone);
                    
                    
                     
                    cliente_cadastrado = cliente1->verificar_registro(lista_de_clientes);

                     
                    if(cliente_cadastrado==false){
                        cout<<"Cliente nao cadastrado, deseja cadastra-lo?"<<endl;
                        string resp;
                        cout<<"Sim - S      Nao - N  :    ";;
                        cin>>resp;
                        if(resp=="S"){
                            lista_de_clientes.push_back(new Cliente(dado1_Nome_cliente,dado2_Cpf,dado3_Telefone));
                            cliente1->cadastro_De_Clientes(*cliente1/*,num*/);
                            //num++;
                        }else
                        {
                             cout<<"Desculpe, a venda so sera autorizada com cadastro!";
                             system("clear");
                        }
                        
                         
                    }else if (cliente_cadastrado==true)
                    {
                         cout<<"Cliente cadastrado, venda autorizada!"<<endl; 
                         
                         cout<<endl; 
                          system("clear");
                         cout<<"O que deseja comprar? Esse sao os produtos que temos no estoque:"<<endl;
                         for (Produto *p:lista_de_produtos){
                            cout<<"------------------------"<<endl;
                            cout<<"Nome: "<< p->get_nome()<< endl;
                            cout<<"Preco: "<< p->get_preco() << endl;
                            cout<<"Quantidade: "<< p->get_quantidade() << endl;
                            cout<<"Categoria: "<< p->get_categoria() << endl;
                            cout<<"Posicao na lista:"<< k <<endl;
                            cout<<"------------------------"<<endl;
                            cout<<endl;
                             
                            k++;
                        }
                         
                        int compra = 1;
                        string cancelar;
                         
                        int posicao;
                        cout<<"Iniciar compra!";
                         
                         while (compra==1)
                         {  
                             
                             Carrinho * carrinho1;
                             carrinho1 = new Carrinho();
                             cout<<"Digite a posicao e a quantidade do produto que deseja adicionar ao carrinho:  ";
                             cin>>posicao;
                             cin>>quant;
                             cout<<endl;
                             cout<<"Adicionando ao carrinho!";
                             j=posicao-1;
                              
                             carrinho1->adicionar_produtos(*lista_de_produtos[j],quant);
                             cout<<endl;
                             cout<<endl;
                            cout<<"Caso deseje continuar, digite O, caso deseje cancelar a compra, digite C, caso deseje finalizar compra digite F:  ";
                            cin>>cancelar;
                            if (cancelar=="C")
                            {
                                carrinho1->cancelar_Compra();
                                compra = 0;
                            } else if(cancelar=="F")
                            {   carrinho1->finalizar_compra(quant);
                                compra=0;

                            }else if(cancelar=="O")
                            {
                                compra=1;
                            }
                            
                            
                             
                              
                         }
                         

                         
                    }else
                    {
                        cout<<"Lista ainda vazia!";
                    }
                    
                    
                     
                
                break;
                }
            case 2:{
                cout<<"\t\t******************************************\n"<<endl;
                cout<<"\t\t* Escolha a acao que deseja realizar:    *\n"<<endl;
                cout<<"\t\t******************************************\n"<<endl;
                cout<<"\t\t*          1-Cadastrar novos produtos    *\n"<<endl;
                cout<<"\t\t*          2-Mostrar dados do produto    *\n"<<endl;
                cout<<"\t\t******************************************\n"<<endl;
                int op2; 
                cout<<"Opcao selecionada:";
                cin>>op2;
                //int i;
                string dado1_Nome_produto;
                double dado2_Preco;
                int dado3_Quantidade;
                string dado4_Categoria;
                int dado5_Id;
                 
                system("clear");
                switch (op2){
                    case 1:
                        cout<<"Insira o nome do Produto: ";
                        cin>>dado1_Nome_produto;
                        cout<<"Insira seu preco: ";
                        cin>>dado2_Preco;
                        cout<<"Insira a quantidade a ser registrada: ";
                        cin>>dado3_Quantidade;
                        cout<<"Categorias possiveis:  |  empacotados   |  resfriados   |   enlatados |"<<endl;
                        cout<<"Insira sua categoria: ";
                        cin>>dado4_Categoria;
                        cout<<"Insira o id deste produto :";
                        cin>>dado5_Id;

                        if( dado4_Categoria==str1){
                            lista_de_produtos.push_back(new Enlatados(dado1_Nome_produto,dado2_Preco,dado3_Quantidade,dado4_Categoria,dado5_Id));
                            Enlatados * enlatado;
                            enlatado = new Enlatados(dado1_Nome_produto,dado2_Preco,dado3_Quantidade,dado4_Categoria,dado5_Id);
                            enlatado->registrar_Produtos(*enlatado);
                        }else if ( dado4_Categoria==str2 ){
                             lista_de_produtos.push_back(new Empacotados(dado1_Nome_produto,dado2_Preco,dado3_Quantidade,dado4_Categoria,dado5_Id));
                            Empacotados * empacotado;
                            empacotado = new Empacotados(dado1_Nome_produto,dado2_Preco,dado3_Quantidade,dado4_Categoria,dado5_Id);
                            empacotado->registrar_Produtos(*empacotado);
                            
                        }else if (dado4_Categoria==str3 ){
                            lista_de_produtos.push_back(new Resfriados(dado1_Nome_produto,dado2_Preco,dado3_Quantidade,dado4_Categoria,dado5_Id));
                            Resfriados * resfriado;
                            resfriado = new Resfriados(dado1_Nome_produto,dado2_Preco,dado3_Quantidade,dado4_Categoria,dado5_Id);
                            resfriado->registrar_Produtos(*resfriado);
                        }
                        
                        
                         
                        break;
                    case 2:
                        for (Produto *p:lista_de_produtos){
                            cout<<"------------------------"<<endl;
                            cout<<"Nome: "<< p->get_nome()<< endl;
                            cout<<"Preco: "<< p->get_preco() << endl;
                            cout<<"Quantidade: "<< p->get_quantidade() << endl;
                            cout<<"Categoria: "<< p->get_categoria() << endl;
                            cout<<"Id: " << p->get_id()<<endl;
                            cout<<"------------------------"<<endl;
                            cout<<endl;

                        }
                        break;
                     
                
                    default:
                    cout<<"Opcao nao encontrada!"<<endl;
                        
                }
                 
            
                
                system("clear");
                
                 
                break;
                }
            case 3:{
                        cout<<"Me desculpe, nao consegui fazer ): !"<<endl;
                break; 
                }      
                default:{
                    cout<<"Selecione uma opcao válida!"<<endl;
                 break;
                }
        }
     }
      
      

    
    }
    return 0;
}