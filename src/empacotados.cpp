#include "empacotados.hpp"

Empacotados::Empacotados(){
     set_nome("");
     set_preco(0.0);
     set_quantidade(0);
     set_categoria("");
     set_id(0);
    this->quantidade_no_estoque = 0;
}
Empacotados::Empacotados(string nome,double preco,int quantidade,string categoria,int id){
     set_nome(nome);
     set_preco(preco);
     set_quantidade(quantidade);
     set_categoria(categoria);
     set_id(id);
     
    this->quantidade_no_estoque = 0;
}
Empacotados::~Empacotados(){

}


 
void Empacotados::registrar_Produtos(Empacotados empacotado){
        std::ofstream saida;
        std::ifstream entrada;

        saida.open("empacotados.txt", std::ios::app );
        if(saida.is_open()){
            saida<<"";
            saida<<get_nome()<<endl;
            saida<<get_preco()<<endl;
            saida<<get_quantidade()<<endl;
            saida<<get_id()<<endl;
        }
        this->quantidade_no_estoque += get_quantidade(); 
}