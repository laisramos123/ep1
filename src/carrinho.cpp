#include "carrinho.hpp"
 
 
Carrinho::Carrinho(){
         this->total=0.0;
}

Carrinho::~Carrinho(){

}
double Carrinho::get_total(){
        return total;
}
void Carrinho::set_total(double total){
        this->total=total;
}
void Carrinho:: cancelar_Compra(){
         this->total=0.0;
         this->produtos.clear();
}
 
void Carrinho::adicionar_produtos(Produto &produto,int quant){
         
         
        double total_produto = produto.get_preco()* quant;

         this->produtos.push_back(new Produto(produto.get_nome(),produto.get_preco(),produto.get_quantidade(),produto.get_categoria(),produto.get_id()));
         this->total += total_produto;  
        

}
  
void Carrinho::finalizar_compra(int quant){
        cout<<"Compra finalizada!Esses sao os itens que voce comprou"<<endl;
        cout<<endl;
        for(Produto *pr : this->produtos){
        cout<<"------------------------"<<endl;
        cout << "  Nome do produto: " << pr->get_nome() << endl;
        cout << "  Preco: " << pr->get_preco() <<"R$" << endl;
        cout << "  Quantidade: " << quant << " unidades"<< endl;
        cout<<"------------------------"<<endl;
        cout << endl;
        

    }
        double valor_com_desconto=0.0;
        valor_com_desconto=0.15*get_total();

        cout<<"Valor total da compra:   "<<get_total()<<" R$"<<endl;
        cout<<"Desconto de socio: 15%"<<endl;
        cout<<"Valor a ser pago : "<<valor_com_desconto<<endl;
        
}

        
